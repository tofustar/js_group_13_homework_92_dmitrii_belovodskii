import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {RegisterComponent} from "./pages/register/register.component";
import {LoginComponent} from "./pages/login/login.component";
import {ChatComponent} from "./pages/chat/chat.component";
import {HomeComponent} from "./pages/home/home.component";
import {TokenGuardService} from "./services/token-guard.service";

const routes: Routes = [
  {path: '', component: HomeComponent},
  {path: 'home', component: HomeComponent},
  {path: 'register', component: RegisterComponent},
  {path: 'login', component: LoginComponent},
  {
    path: 'chat',
    component: ChatComponent,
    canActivate: [TokenGuardService],
    data: {token: [true]}
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
