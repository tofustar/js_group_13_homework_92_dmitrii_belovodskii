const {nanoid} = require('nanoid');
const User = require("../models/User");

const activeConnections = {};
const savedMessages = [];

module.exports = (ws, req) => {
  const id = nanoid();
  activeConnections[id] = ws;

  ws.send(JSON.stringify({
    type: 'PREV_MESSAGES',
    messages: savedMessages
  }));

  let user = null;

  ws.on('message', async (msg) => {
    const decodedMessage = JSON.parse(msg);
    switch (decodedMessage.type) {
      case 'LOGIN':
        const filter = {};

        if (decodedMessage.token) {
          filter.token = decodedMessage.token;
        } else {
          break;
        }

        user = await User.findOne(filter);

        ws.send(JSON.stringify({
          type: 'LOGIN_USER',
          message: user.displayName,
        }));

        break;
      case 'SEND_MESSAGE':
        if (user === null) {
          break
        }
        Object.keys(activeConnections).forEach(id => {
          const conn = activeConnections[id];
          conn.send(JSON.stringify({
            type: 'NEW_MESSAGE',
            message: {
              user,
              text: decodedMessage.text,
            }
          }));
        });
        break;
      default:
        console.log('Unknown type:', decodedMessage.type)
    }
  });

  ws.on('close', () => {
    console.log('client disconnected! id=', id);
    delete activeConnections[id];
  })
};