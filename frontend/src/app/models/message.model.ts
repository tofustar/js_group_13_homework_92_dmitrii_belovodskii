import {User} from "./user.model";

export interface Message {
  user: User,
  text: string,
}

export interface ServerMessage {
  type: string,
  messages: Message[],
  message: Message
}