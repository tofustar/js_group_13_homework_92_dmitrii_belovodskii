import {AfterViewInit, Component, OnDestroy} from '@angular/core';
import {Message, ServerMessage} from "../../models/message.model";
import {User} from "../../models/user.model";
import {AppState} from "../../store/types";
import {Store} from "@ngrx/store";
import {Observable} from "rxjs";
import {logoutUserRequest} from "../../store/users.actions";

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.sass']
})
export class ChatComponent implements AfterViewInit, OnDestroy {
  ws!: WebSocket;
  user: Observable <null |User>;
  messageText = '';
  messages: Message[] = [];
  users: User[] = [];
  token: string | undefined = '';

  constructor(private store: Store<AppState>) {
    this.user = store.select(state => state.users.user);
  }

  ngAfterViewInit() {

    this.user.subscribe(user => {
      this.users.push(user!);
      this.token = user?.token;
    });

    this.ws = new WebSocket('ws://localhost:8000/chat');
    this.ws.onclose = () => console.log('ws closed');

    this.ws.onopen = () => {
      this.ws.send(JSON.stringify({
        type: 'LOGIN',
        token: this.token
      }));
    }

    this.ws.onmessage = event => {
      const decodedMessage: ServerMessage = JSON.parse(event.data);

      if(decodedMessage.type === 'PREV_MESSAGES') {
        decodedMessage.messages.forEach(msg => {
          this.messages.push(msg);
        });
      }

      if(decodedMessage.type === 'NEW_MESSAGE'){
        this.messages.push(decodedMessage.message);
      }

      if(decodedMessage.type === 'LOGIN_USER') {
        // this.users.push(decodedMessage.message);
      }
    }
  }

  ngOnDestroy() {
    this.store.dispatch(logoutUserRequest());
    this.ws.close();
  }

  sendMessage() {
    this.ws.send(JSON.stringify({
      type: 'SEND_MESSAGE',
      text: this.messageText,
    }));
    this.messageText = '';
  }
}
