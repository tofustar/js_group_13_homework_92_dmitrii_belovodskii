const rootPath = __dirname;

module.exports = {
  rootPath,
  mongo: {
    db:'mongodb://localhost/chat-messenger',
    options: {useNewUrlParser: true},
  }
};