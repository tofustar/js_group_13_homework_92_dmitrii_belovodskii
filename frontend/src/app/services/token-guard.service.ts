import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot} from "@angular/router";
import {map, Observable} from "rxjs";
import {Injectable} from "@angular/core";
import {User} from "../models/user.model";
import {AppState} from "../store/types";
import {Store} from "@ngrx/store";

@Injectable({
  providedIn: 'root'
})

export class TokenGuardService implements CanActivate {
  user: Observable<null | User>

  constructor(private store: Store<AppState>, private router: Router) {
    this.user = store.select(state => state.users.user);
  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {

    return this.user.pipe(
      map(user => {
        if (user) {
          return true;
        }

        void this.router.navigate(['/']);
        return false;
      })
    );
  }
}